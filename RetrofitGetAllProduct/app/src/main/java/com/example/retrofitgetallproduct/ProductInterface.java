package com.example.retrofitgetallproduct;

import java.net.URL;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ProductInterface {
public static final String BASE_URL = "http://10.0.2.2//android/";
@GET("get_all_products.php")
 Call<Products> getProducts();
        // Customer is POJO class to get the data from API,
        // use Call<T> in callback
        // because the data in our API is starting from JSONObject
        }
