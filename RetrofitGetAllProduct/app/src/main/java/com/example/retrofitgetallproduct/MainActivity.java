package com.example.retrofitgetallproduct;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listViewProducts);
        //defining a progress dialog to show while loading
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data...");
        progressDialog.show();
        // 1. Building retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ProductInterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // 2. Defining retrofit api service
        ProductInterface productInterface =
                retrofit.create(ProductInterface.class);
        // 3. Defining the call
        Call<Products> call = productInterface.getProducts();
        // 4. Calling the api as synchronous request
        //Create a retrofit2.Callback object and
        //override it’s onResponse and onFailure method.
        call.enqueue(new Callback<Products>() {
            @Override
            public void onResponse(Call<Products> call, Response<Products>
                    response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    // 5. Converted JSON result stored in response.body()
// and put response data to customer list
                    List<Product> productList =
                            response.body().getProducts();
                    Log.d("getAllProduct", response.body().toString());
                    // 6. Creating an String array for the ListView
                    String[] products = new String[productList.size()];
                    //7. looping through all the customers and inserting thenames inside the string array
                    for (int i = 0; i < productList.size(); i++) {
                        products[i] = productList.get(i).getName();
                    }
                    //8. displaying the string array into listview
                    listView.setAdapter(new
                            ArrayAdapter<String>(getApplicationContext(), R.layout.listrow, R.id.tvList,
                            products));
                }
            }
            @Override
            public void onFailure(Call<Products> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(),
                        Toast.LENGTH_LONG).show();
                Log.e("retrofitreponse",t.getMessage());
            }
        });

    }
    }
